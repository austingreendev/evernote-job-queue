# Evernote-Job-Queue
This content can be found at: [http://evernote.austingreen.info/](http://evernote.austingreen.info/)

**There is a 10 second delay with every job to better show how processing occurs.**

**Due to limited time I did not include any testing. By using the TypeScript interfaces provided it would be relatively easy to create mocks with more time.**

## How to Run
```
git clone https://gitlab.com/austingreenkansas/evernote-job-queue.git
cd evernote-job-queue

# Install npm dependencies
npm Install

# Compiles Typescript and serves content
# Automatically creates SQLite database
# Requires redis to be running at: 127.0.0.1:6379
npm start
```

The Job Queue will begin running at [http://localhost:3000/](http://localhost:3000/) by default.

## Technologies
* Running in a t2.micro instance using AWS Lightsail
* Source Repository can be found here: [https://gitlab.com/austingreenkansas/evernote-job-queue](https://gitlab.com/austingreenkansas/evernote-job-queue)
* Built in Node.js and Express using TypeScript 2.0
* Uses the Kue package to manage job concurrency in Redis
* For these examples I will always use a temporary in-memory instance on the same machine. In a production environment this would be separate.
* Uses SQLite as the persistent store for completed jobs

## Endpoints
You can view an interactive dashboard to view active/stalled/completed jobs at [http://evernote.austingreen.info/queue/](http://evernote.austingreen.info/queue/)

Job Endpoints:

`GET` */job?url=http://www.example.com* - Creates job to parse given url (should be a *POST*, but is *GET* for easy testing from browser)
* Expects 'url' parameter.
* Example: "/job?url=http://www.example.com"
* Response: If the url is valid you will receive an object similar to {"jobId": "beeaed71-87ba-40d1-8d12-ab336a3f4fb1"}
* [Click here to create a valid job (http://austingreen.info)](http://evernote.austingreen.info/job?url=http://austingreen.info)
* [Click here to create an invalid job](http://evernote.austingreen.info/job?url=thisshouldbeinvalid)

`GET` */job/:unique-job-id* - View associated queueJob and status/result
* Expects ':unique-job-id' url parameter.
* Example: "/job/beeaed71-87ba-40d1-8d12-ab336a3f4fb1"
* Response: If the url is valid you will receive an object similar to

`GET` */queue* - View Kue dashboard with live updating

## Discussion
* Node.js
    * I chose to use Node.js since it is very easy to spin up a quick application using Express and
has excellent support for asynchronously accessing different storage mediums.
* TypeScript 2.0
    * I chose TypeScript because of it's ability to easily include interface files using "@types/"
for code-completion and type checking across all of my libraries.
    * Additionaly, using TypeScript allowed me to write unit tests that test my business logic and
not worry about "piping" code.
* Kue (Redis)
    * Kue is a utility that I have used in the past for simple job processing [https://github.com/Automattic/kue](https://github.com/Automattic/kue)
    * Uses Redis as a store for jobs and their status/priority.
* SQLite
    * I chose SQLite as my persistent store because it is very easy to manipulate using prepared statements.
Also, it doesn't require me to setup/configure a local Postgres or MySQL engine.
    * For this example, the Schema is a simple: "uuid (TEXT), jobObject (TEXT), html (TEXT), error (TEXT)".

## Screenshots
Screenshots are visible at [http://evernote.austingreen.info/](http://evernote.austingreen.info/)

